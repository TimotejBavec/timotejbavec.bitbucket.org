
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}

function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}

function preberiEHRodBolnika() {
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', rojen '" + party.dateOfBirth +
          "'.</span>");
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
	}
}

function dodajMeritveVitalnihZnakov() {
	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'>" +
          res.meta.href + ".</span>");
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}



function preberiMeritveVitalnihZnakov() {
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
            "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");
  				if (tip == "telesna temperatura") {
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].temperature +
                      " " + res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				} else if (tip == "telesna teža") {
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight + " " 	+
                      res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}


$(document).ready(function() {

  
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
	});

  
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});


window.addEventListener('load',function(){
  if($('#mapid').length){
    var mymap = L.map('mapid').setView([46.0569, 14.5058], 14);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    id: 'mapbox.streets'
  }).addTo(mymap);
  L.polygon([
		[
          [
            [
              46.0536077,
              14.5247825
            ],
            [
              46.0536599,
              14.5247114
            ],
            [
              46.053729,
              14.5248169
            ],
            [
              46.0539881,
              14.5244643
            ],
            [
              46.0536582,
              14.5239608
            ],
            [
              46.0533946,
              14.5243194
            ],
            [
              46.0534379,
              14.5243854
            ],
            [
              46.0533901,
              14.5244505
            ],
            [
              46.0536077,
              14.5247825
            ]
          ],
          [
            [
              46.0536583,
              14.5242656
            ],
            [
              46.0537743,
              14.5244439
            ],
            [
              46.0536733,
              14.5245804
            ],
            [
              46.0535573,
              14.5244021
            ],
            [
              46.0536583,
              14.5242656
            ]
          ]
        ]
	]).addTo(mymap).bindPopup("Porodnišnica Ljubljana<br />Šlajmerjeva ulica 4, Ljubljana");
	L.polygon([
          [
            [
              46.3766812,
              14.2019099
            ],
            [
              46.3767532,
              14.2018207
            ],
            [
              46.3767282,
              14.2017822
            ],
            [
              46.3769595,
              14.2014111
            ],
            [
              46.3770294,
              14.2014902
            ],
            [
              46.3770822,
              14.2013965
            ],
            [
              46.376993,
              14.2012999,
            ],
            [
              46.3769211,
              14.2012161
            ],
            [
              46.3769047,
              14.2012388
            ],
            [
              46.3767647,
              14.201026
            ],
            [
              46.3769683,
              14.2007415
            ],
            [
              46.3771387,
              14.200969
            ],
            [
              46.3770278,
              14.201149
            ],
            [
              46.3770631,
              14.201197
            ],
            [
              46.3771717,
              14.2010137
            ],
            [
              46.3771783,
              14.2010233
            ],
            [
              46.3772252,
              14.2009539
            ],
            [
              46.3769183,
              14.2005271
            ],
            [
              46.3766962,
              14.2008704
            ],
            [
              46.376674,
              14.2008382
            ],
            [
              46.3765279,
              14.2010635
            ],
            [
              46.3765519,
              14.2010957
            ],
            [
              46.3763668,
              14.2013505
            ],
            [
              46.3766812,
              14.2019099
            ]
          ],
          [
            [
              46.3769176,
              14.2013551
            ],
            [
              
              46.3766938,
              14.2017212
            ],
            [
              
              46.3765444,
              14.2014709
            ],
            [
              46.3766685,
              14.2012884
            ],
            [
              46.3767823,
              14.201445
            ],
            [
              46.3768764,
              14.2013018
            ],
            [
              46.3769176,
              14.2013551
            ]
          ]
        ]
	).addTo(mymap).bindPopup("Psihiatrična bolnišnica<br />Begunje na Gorenjskem 55, Begunje na Gorenjskem");
	L.polygon([
          [
            [
              45.7722339,
              14.2105347
            ],
            [
              45.7719932,
              14.2101776
            ],
            [
              45.7718945,
              14.2103143
            ],
            [
              45.7719242,
              14.2103583
            ],
            [
              45.7716037,
              14.2108022
            ],
            [
              45.7719386,
              14.211299
            ],
            [
              45.7720331,
              14.2111681
            ],
            [
              45.7719829,
              14.2110936
            ],
            [
              45.7720047,
              14.2110634
            ],
            [
              45.7720357,
              14.2111094
            ],
            [
              45.7723602,
              14.2106598
            ],
            [
              45.7722556,
              14.2105046
            ],
            [
              45.7722339,
              14.2105347
            ]
          ],
          [
            [
              45.7721322,
              14.2106739
            ],
            [
              45.7719203,
              14.2109719
            ],
            [
              45.771821,
              14.2108269
            ],
            [
              45.7720281,
              14.2105356
            ],
            [
              45.772056,
              14.2105765
            ],
            [
              45.7720416,
              14.2105967
            ],
            [
              45.7720875,
              14.2106638
            ],
            [
              45.7721067,
              14.2106368
            ],
            [
              45.7721322,
              14.2106739
            ]
          ]
        ]
	).addTo(mymap).bindPopup("Zdravstveni dom<br />Prečna ulica 2, Postojna");
	L.polygon([
          [
            [
              46.0690035,
              14.487361
            ],
            [
              46.0687695,
              14.4875938
            ],
            [
              46.0687325,
              14.4875056
            ],
            [
              46.0685928,
              14.4876547
            ],
            [
              46.0686171,
              14.4877262
            ],
            [
              46.0685575,
              14.4877856
            ],
            [
              46.0685258,
              14.4877141
            ],
            [
              46.0682338,
              14.4880131
            ],
            [
              46.0683489,
              14.4882172
            ],
            [
              46.068419,
              14.4881448
            ],
            [
              46.0684094,
              14.4881255
            ],
            [
              46.0685392,
              14.4879995
            ],
            [
              46.0686173,
              14.4881562
            ],
            [
              46.0686917,
              14.4880701
            ],
            [
              46.0687151,
              14.4880949
            ],
            [
              46.0687971,
              14.4880147
            ],
            [
              46.0688406,
              14.4879697
            ],
            [
              46.0687744,
              14.4878184
            ],
            [
              46.0690752,
              14.4875057
            ],
            [
              46.0690035,
              14.487361
            ]
          ]
        ]
	).addTo(mymap).bindPopup("Zdravstveni dom Šiška<br />Derčeva ulica 5, Ljubljana");
	L.polygon([
          [
            [
              46.0680191,
              14.4867966
            ],
            [
              46.0678885,
              14.4867367
            ],
            [
              46.0677967,
              14.4871527
            ],
            [
              46.0677721,
              14.4871414
            ],
            [
              46.0677298,
              14.487122
            ],
            [
              46.0677419,
              14.4870673
            ],
            [
              46.0675083,
              14.4869602
            ],
            [
              46.0675853,
              14.4866109
            ],
            [
              46.067439,
              14.4865438
            ],
            [
              46.0672259,
              14.487509
            ],
            [
              46.0673709,
              14.4875755
            ],
            [
              46.0674642,
              14.4871523
            ],
            [
              46.0677016,
              14.4872611
            ],
            [
              46.0677162,
              14.4871947
            ],
            [
              46.0677773,
              14.4872227
            ],
            [
              46.067695,
              14.4875958
            ],
            [
              46.0678307,
              14.487658
            ],
            [
              46.0679112,
              14.4872933
            ],
            [
              46.067935,
              14.4873042
            ],
            [
              46.0679822,
              14.4870904
            ],
            [
              46.0679569,
              14.4870788
            ],
            [
              46.0680191,
              14.4867966
            ]
          ]
        ]
	).addTo(mymap).bindPopup("Bolnica Dr. Petra Držaja<br />Vodnikova cesta 62, Ljubljana");
	L.polygon([
          [
            [
              46.0548693,
              14.5202926
            ],
            [
              46.0550389,
              14.5202817
            ],
            [
              46.0550382,
              14.5202407
            ],
            [
              46.0550318,
              14.520052
            ],
            [
              46.0546373,
              14.5200774
            ],
            [
              46.0545917,
              14.5200804
            ],
            [
              46.0545954,
              14.5202024
            ],
            [
              46.0542401,
              14.5202253
            ],
            [
              46.054241,
              14.5202945
            ],
            [
              46.0542459,
              14.5204115
            ],
            [
              46.0536717,
              14.5204485
            ],
            [
              46.0532755,
              14.520474
            ],
            [
              46.0532964,
              14.5211471
            ],
            [
              46.0536512,
              14.5211243
            ],
            [
              46.0536579,
              14.5213389
            ],
            [
              46.0536626,
              14.5214908
            ],
            [
              46.0536679,
              14.5216636
            ],
            [
              46.0537365,
              14.5216591
            ],
            [
              46.0537423,
              14.5218437
            ],
            [
              46.053819,
              14.5218388
            ],
            [
              46.0538248,
              14.5220228
            ],
            [
              46.0539105,
              14.5220173
            ],
            [
              46.0539165,
              14.5222127
            ],
            [
              46.0543577,
              14.5221843
            ],
            [
              46.0543243,
              14.5211053
            ],
            [
              46.0547162,
              14.52108
            ],
            [
              46.0547187,
              14.5211605
            ],
            [
              46.0545768,
              14.5211697
            ],
            [
              46.0545823,
              14.5213448
            ],
            [
              46.054734,
              14.521335
            ],
            [
              46.0547432,
              14.5216308
            ],
            [
              46.0548721,
              14.5216225
            ],
            [
              46.0548537,
              14.5210313
            ],
            [
              46.0548922,
              14.5210289
            ],
            [
              46.0548693,
              14.5202926
            ]
          ]
        ]
	).addTo(mymap).bindPopup("Univerzitetni klinični center Ljubljana<br />Zaloška cesta 7, Ljubljana");
	L.polygon(
	  [
          [
            [
              46.0532476,
              14.5234539
            ],
            [
              46.0531321,
              14.5236093
            ],
            [
              46.0532336,
              14.5237658
            ],
            [
              46.053272,
              14.5238251
            ],
            [
              46.0534272,
              14.5240645
            ],
            [
              46.0535427,
              14.5239091
            ],
            [
              46.0533323,
              14.5235845
            ],
            [
              46.0532476,
              14.5234539
            ]
          ]
        ]
	).addTo(mymap).bindPopup("Leonišče");
	L.polygon([
          [
            [
              46.2324647,
              15.260609
            ],
            [
              46.2326283,
              15.2613309
            ],
            [
              46.2328545,
              15.262066
            ],
            [
              46.2328732,
              15.2622397
            ],
            [
              46.2328373,
              15.2629958
            ],
            [
              46.232947,
              15.2630836
            ],
            [
              46.2343905,
              15.2632485
            ],
            [
              46.2346238,
              15.2630892
            ],
            [
              46.2352335,
              15.2596943
            ],
            [
              46.2347512,
              15.2596213
            ],
            [
              46.2342589,
              15.2594928
            ],
            [
              46.2325314,
              15.2604485
            ],
            [
              46.2324647,
              15.260609
            ]
          ]
        ]
	).addTo(mymap).bindPopup("Splošna bolnišnica Celje<br />Oblakova ulica, Celje");
L.polygon([
          [
            [
              46.0518964,
              14.5693615
            ],
            [
              46.051813,
              14.5696018
            ],
            [
              46.0520239,
              14.5697171
            ],
            [
              46.0520802,
              14.5699761
            ],
            [
              46.0519772,
              14.5700275
            ],
            [
              46.0520155,
              14.5701769
            ],
            [
              46.0520747,
              14.570154
            ],
            [
              46.0520843,
              14.5701916
            ],
            [
              46.0523779,
              14.5700591
            ],
            [
              46.0523711,
              14.5700334
            ],
            [
              46.0524273,
              14.5700057
            ],
            [
              46.0523889,
              14.5698278
            ],
            [
              46.0523299,
              14.5698614
            ],
            [
              46.0522776,
              14.5698765
            ],
            [
              46.0522299,
              14.5696276
            ],
            [
              46.0523312,
              14.5693615
            ],
            [
              46.0521644,
              14.5692499
            ],
            [
              46.052087,
              14.5694817
            ],
            [
              46.0518964,
              14.5693615
            ]
          ]
        ]
	).addTo(mymap).bindPopup("Psihiatrična klinika Ljubljana<br />Ljubljana");
	L.polygon([
          [
            [
              45.9376381,
              14.8054871
            ],
            [
              45.9375304,
              14.8055031
            ],
            [
              45.937544,
              14.8056929
            ],
            [
              45.937567,
              14.8056895
            ],
            [
              45.9375908,
              14.8060217
            ],
            [
              45.9376967,
              14.806006
            ],
            [
              45.9376733,
              14.8056795
            ],
            [
              45.9376521,
              14.8056827
            ],
            [
              45.9376381,
              14.8054871
            ]
          ]
        ]
	).addTo(mymap).bindPopup("Ivančna Gorica<br />Ivančna Gorica");
}
});


